# Demonstrate docker common image layers


Step 1: Create Directories and Dockerfiles
Create two separate directories, let's call them dir1 and dir2. In each directory, create a file named Dockerfile with the following content:

Directory dir1:

```
# Dockerfile in dir1
FROM alpine:3.17.3
RUN apk update && apk add --no-cache curl
```
Directory dir2:

```
# Dockerfile in dir2
FROM alpine:3.17.3
RUN apk update && apk add --no-cache jq
```
Step 2: Build Docker Images
Navigate to dir1. Run the following command to build the first Docker image:

```
docker build -t myimage1:1 .
```
Next, navigate to dir2 in the terminal and build the second Docker image using the following command:

```
docker build -t myimage2:1 .
```
Step 3: Demonstrate Common Image Layers
To demonstrate common image layers between the two Docker images, you can use the docker history command. Run the following commands:

For myimage1:1:

```
docker history myimage1:1
```
For myimage2:1:

```
docker history myimage2:1
```
The docker history command will display the individual layers of each image, and you will notice that the base image layer (alpine:3.17.3) is common between them.


---
# Tag and push images




Step 1: Tag the Images
Open a terminal and run the following commands to tag the images:

For myimage1:1:

```
docker tag myimage1:1 mydockerusername/image1:1.0
```
For myimage2:1:

```
docker tag myimage2:1 mydockerusername/image2:1.0
```
Step 2: Log in to Docker Hub
Before pushing the images, make sure you are logged in to Docker Hub using your Docker Hub username and password. Run the following command and enter your credentials when prompted:

```
docker login
```
Step 3: Push the Images to Docker Hub
Once you are logged in to Docker Hub, run the following commands to push the tagged images:

For mydockerusername/image1:1.0:

```
docker push mydockerusername/image1:1.0
```
For mydockerusername/image2:1.0:

```
docker push mydockerusername/image2:1.0
```
These commands will push the tagged images to your Docker Hub account. Make sure to replace mydockerusername with your actual Docker Hub username.

After successfully pushing the images, they will be available on Docker Hub for you to use and share with others.

Go to Docker hub and examine your new images. Check the tags and see that the alpine:3.17.3 layer is part of your images.

How many times is alpine:3.17.3 stored at Docker hub?

