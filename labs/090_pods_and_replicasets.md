# ReplicaSets

Step 1: Create a file named nginx-rs.yaml and add the following content:

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: nginx-rs
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
        - name: nginx
          image: nginx
```
Step 2: Create the ReplicaSet configuration:


```
kubectl create -f nginx-rs.yaml
```
Step 3: Verify the ReplicaSet and the associated pods:


```
kubectl get replicaset
kubectl get pods
```

Step 4: Scale the ReplicaSet to a different number of replicas. Modify the replicas field in nginx-rs.yaml to the desired number, and apply the changes again:


```
kubectl apply -f nginx-rs.yaml
```

Step 5: Verify the updated number of replicas:


```
kubectl get replicaset
kubectl get pods
```

Step 6: Delete pods
```
kubectl delete pod nginx-rs-USE_THE_CORRECT_POD_NAME 
```

Step 7: Examine pods again
```
kubectl get pods
```

Step 8: Let's take a closer look
```
kubectl get -o wide rs nginx-rs
kubectl get --show-labels pod
kubectl describe pod | less -i
```
# What happened?

Step 9: Scale using imperative command:
```
kubectl scale replicaset nginx-rs --replicas 1
kubectl get rs
kubectl get pods
```