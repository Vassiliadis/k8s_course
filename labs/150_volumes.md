Step 1: Create a Pod YAML file with emptyDir and hostPath volumes:
Create a file named volumes-demo.yaml with the following content:


```
apiVersion: v1
kind: Pod
metadata:
  name: volumes-demo
spec:
  containers:
    - name: nginx-container
      image: nginx
      volumeMounts:
        - name: emptydir-volume
          mountPath: /data/emptydir
        - name: hostpath-volume
          mountPath: /data/hostpath
  volumes:
    - name: emptydir-volume
      emptyDir: {}
    - name: hostpath-volume
      hostPath:
        path: /path/on/host
```
In the above YAML, we define a Pod named volumes-demo with an nginx container. The Pod has two volumes:

emptyDir-volume: An empty directory volume that is created when the Pod is scheduled on a node and exists for the duration of the Pod's lifecycle.

hostPath-volume: A hostPath volume that mounts a directory from the host machine into the Pod. Adjust the path field to the desired directory path on the host machine.

Step 2: Create the Pod:

```
kubectl create -f volumes-demo.yaml
```
This command creates the Pod in the cluster.

Step 3: Verify the Pod:


```
kubectl get pods
```
You should see the volumes-demo Pod in the running state.

Step 4: Access the container and interact with the volumes:

```
kubectl exec -it volumes-demo -- /bin/bash

mkdir -v /data/emptydir/$RANDOM # create a directory
mkdir -v /data/hostpath/$RANDOM
 
 
```
This command opens a shell in the volumes-demo container.

Inside the container shell, you can navigate to the /data/emptydir and /data/hostpath directories and perform operations like creating files or directories.

Exit the container shell:

```
exit
```
Step 5: Clean up:

```
kubectl delete pod volumes-demo
```
This command deletes the volumes-demo Pod.


By following these steps, you have created a Pod with emptyDir and hostPath volumes. The emptyDir volume is ephemeral and tied to the lifecycle of the Pod, while the hostPath volume allows you to access and manipulate files on the host machine from within the Pod.

---
Can you identify any problem using hostPath volumes?
