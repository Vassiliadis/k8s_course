Step 1: Create a file named Dockerfile (without any file extension) and add the following content:

```
FROM alpine:latest

# Set an environment variable
ENV MY_VARIABLE=my-value

# Print the value of the environment variable
CMD echo "The value of MY_VARIABLE is: $MY_VARIABLE"
```
Step 2: Build the Docker image using the Dockerfile:

```
docker build -t my-alpine-demo .
```
Step 3: Run the Docker container and pass a value to the environment variable using the -e flag:

```
docker run -e MY_VARIABLE=my-custom-value my-alpine-demo
```
Replace my-custom-value with the desired value you want to assign to the MY_VARIABLE environment variable.

You should see the output showing the value of the environment variable as specified in the CMD instruction in the Dockerfile.

By using the -e flag followed by the environment variable name and value, you can pass variables to the container's environment at runtime. The container then accesses and uses these environment variables as required.

Step 4: run a new container using debian:11:
```
docker run -e "a=`date`" -it docker.io/library/debian:11
echo $a
```

