# RBAC

In this lab we will authorize user 'user' to run kubectl in order to access the Kubernetes API so it can gather information about pods in the default namespace.

Create and execute the following shell script:
```
#!/bin/sh

mkdir /home/user/.kube
sed -n '1,14p' /root/.kube/config > /home/user/.kube/config
chmod 0700 /home/user/.kube
chmod 0600 /home/user/.kube/config

# create serviceaccount
kubectl create serviceaccount pod-reader

# create role
kubectl create role pod-reader --verb=get --verb=list --verb=watch --resource=pods

# bind role and serviceaccount
kubectl create rolebinding pod-reader-rolebinding --serviceaccount default:pod-reader --role pod-reader

# create the token we'll use to authenticate ourselves to Kubernetes
kubectl create token pod-reader --duration 240h > /home/user/.kube/token

chmod 0600 /home/user/.kube/token

# delegate ownership to user 'user'
chown -R user:user /home/user/.kube
```


Check files /root/.kube/config and /home/user/.kube/config. Notice the differences!


Now, we can test our configuration:
```
# run two pods
kubectl run --image nginx nginx-0
kubectl run --image nginx nginx-1
su -l user # switch to user 'user' 


kubectl get --watch pod # should fail
kubectl get --watch -n kube-system pod # should fail

kubectl get --watch --token $(cat ~/.kube/token) pod # should not fail
# switch to another terminal and create or delete a few pods
kubectl get --watch --token $(cat ~/.kube/token) -n kube-system pod # should fail
```

Use 'describe' clusterrole to get an idea:
```
kubectl describe clusterrole | less
```
Now, edit the role and try something on your own:
```
kubectl edit role pod-reader
```
