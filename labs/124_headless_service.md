Create a few pods with a custom label:
```
kubectl run frontend0 --labels app=front --image=nginx:1.25
kubectl run frontend1 --labels app=front --image=nginx:1.25
```

Verify pods and attached labels:
```
kubectl get pod --show-labels
```

Create a new service, using "ClusterIP: None" and a selector to route traffic to our pods:
```
kubectl apply -f - <<EOF
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: custom-svc-for-my-two-pods
  name: custom-svc-for-my-two-pods
spec:
  ports:
  - name: "80"
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: front
  type: ClusterIP
  clusterIP: None
EOF
```



Verify our new service:
```
kubectl get svc custom-svc-for-my-two-pods
```
It should look like this:
```
NAME                         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
custom-svc-for-my-two-pods   ClusterIP   None         <none>        80/TCP    38s
```

There is no Cluster IP!



Examine further. Let's run a pod: 
```
kubectl run --image ubuntu:23.04 -it --image-pull-policy IfNotPresent --restart Never ubuntu-$RANDOM

apt update # update pkg list
apt install -y curl bind9-host # install tools

host custom-svc-for-my-two-pods
exit # leave the Ubuntu pod

kubectl get -o wide pod frontend0 frontend1
```

Observe that the "custom-svc-for-my-two-pods" DNS name resolves directly to our pods instead of an intermediate IP address.  
Something like:  
```
root@ubuntu-2973:/# host custom-svc-for-my-two-pods
custom-svc-for-my-two-pods.default.svc.cluster.local has address 10.244.1.76
custom-svc-for-my-two-pods.default.svc.cluster.local has address 10.244.2.78
root@ubuntu-2973:/#
exit
root@debian130.manager:~$ kubectl get -o wide pod frontend0 frontend1
NAME        READY   STATUS    RESTARTS   AGE   IP            NODE                          NOMINATED NODE   READINESS GATES
frontend0   1/1     Running   0          41m   10.244.2.78   debian132.worker.none.local   <none>           <none>
frontend1   1/1     Running   0          41m   10.244.1.76   debian131.worker.none.local   <none>           <none>
root@debian130.manager:~$
```


Examine our service:
```
kubectl describe svc custom-svc-for-my-two-pods
```

Create more pods with the custom label:
```
kubectl run frontend10 --labels app=front --image=nginx:1.25
kubectl run frontend11 --labels app=front --image=nginx:1.25
```

And examine again:
```
kubectl describe svc custom-svc-for-my-two-pods
```

```
kubectl delete pod frontend10 # delete all but one frontend pod
```

And examine again:
```
kubectl describe svc custom-svc-for-my-two-pods
```



Remember that normal ClusterIP services create a single IP address and a single DNS entry and do load balancing between the backend pods. 

Having a direct way to communicate with a backend pod opens up new possibilities. Now, pods can connect to each other using a DNS name, because the service's DNS entry contains all pods IPs.


