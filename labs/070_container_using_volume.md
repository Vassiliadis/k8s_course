Create a temporary directory and cd into it:
```
cd $(mktemp -d)
```

Create this Dockerfile:
```
FROM mariadb:latest

# Set the environment variables for MariaDB
ENV MYSQL_ROOT_PASSWORD=mysecretpassword
ENV MYSQL_DATABASE=mydatabase

# Start the MariaDB server
CMD ["mariadbd"]
```

In this demo:

We start with the official mariadb:latest image, which provides the MariaDB database server.  
The ENV instructions set the environment variables for MariaDB. In this example, we set the root password to mysecretpassword and create a database named mydatabase.
The VOLUME instruction specifies a bind mount at /var/lib/mysql. This allows the data directory of MariaDB to be mounted from the host machine, providing persistence for the database files.  
Finally, the CMD instruction starts the MariaDB server using the mysqld command.  
To build and run the Docker image:

Open a terminal and navigate to the directory containing the Dockerfile.  
Build the Docker image using the command:

```
docker build -t my-mariadb .
```
Create a directory on the host machine to serve as the bind mount for data. For example:

```
mkdir -p /path/to/host/data
```
Run the Docker container, mapping the container's port 3306 to a port on the host and mounting the bind mount for data:

```
docker run -d -p 3306:3306 -v /path/to/host/data:/var/lib/mysql my-mariadb
```
Now, the MariaDB server is running in the Docker container, and the data directory is bound to the /path/to/host/data directory on the host machine. Any data changes made by the database will be persisted in the bind mount, allowing for data persistence even if the container is stopped or removed.

Now, install the mariadb client to the host:
```
apt install mariadb-client
```

And try to access the database server using:
```
mysql -h 127.0.0.1 -uroot -p
```
Create a database, list databases and exit:
```
create database test1565;
show databases;
exit;
```

Stop the container:
```
# docker ps
CONTAINER ID   IMAGE        COMMAND                  CREATED         STATUS         PORTS                                       NAMES
28dfa65bd662   my-mariadb   "docker-entrypoint.s…"   3 minutes ago   Up 3 minutes   0.0.0.0:3306->3306/tcp, :::3306->3306/tcp   suspicious_pike
# docker stop suspicious_pike
suspicious_pike
```

Run again a container using: 
```
docker run -d -p 3306:3306 -v /path/to/host/data:/var/lib/mysql my-mariadb
```

And try to access the database you created before. Is it there?

Examine the local path:
```
ls -l /path/to/host/data/
```

This is how we can save state in a Docker container.



