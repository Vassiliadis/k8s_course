


Step 1: Deploy a Sample Application

For this tutorial, let's create a simple application to simulate CPU load. We'll deploy an nginx web server with a CPU-intensive container.

```
# nginx-deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        resources:
          limits:
            cpu: "200m"  # Requests and limits for CPU usage
```
Deploy the nginx application:

```
kubectl apply -f nginx-deployment.yaml
```
Step 2: Expose the Application

Expose the nginx deployment as a service so that we can access it externally.

```
# nginx-service.yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
spec:
  selector:
    app: nginx
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
  type: NodePort  # Change this to LoadBalancer or ClusterIP as needed
```

Deploy the service:

```
kubectl apply -f nginx-service.yaml
```
Step 3: Create an HPA

Now, let's create the Horizontal Pod Autoscaler (HPA) for the nginx deployment. We'll scale the deployment based on CPU utilization.

```
# nginx-hpa.yaml
apiVersion: autoscaling/v2
kind: HorizontalPodAutoscaler
metadata:
  name: nginx-hpa
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: nginx-deployment
  minReplicas: 1  # Minimum number of replicas
  maxReplicas: 5  # Maximum number of replicas
  metrics:
    - type: Resource
      resource:
        name: cpu
        target:
          type: Utilization
          averageUtilization: 20  #  CPU percentage
```

Create the HPA:

```
kubectl apply -f nginx-hpa.yaml
```

Step 4: Monitor the HPA

You can monitor the HPA's behavior by checking its status:

```
kubectl get hpa --watch
```
The output will show the current and target replica counts, as well as the utilization metrics.

Step 5: Generate Load

To see the HPA in action, we need to generate load on the nginx deployment. You can use a tool like Apache Benchmark (ab) to simulate traffic. Create a new SSH session to the manager and run the following:

```
# Install Apache Benchmark (if not already installed)
apt install apache2-utils


# Generate load (replace 30281 with the actual service port)
ab -r -t 100 -c 1000 "http://$(hostname):30281/"
```

Observe changes to the HPA on the old terminal running 'kubectl get hpa --watch'.
