Step 1: Create a Deployment YAML file
Create a file named demo-deployment.yaml and add the following content:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: demo-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: demo
  template:
    metadata:
      labels:
        app: demo
    spec:
      containers:
      - name: debian
        image: debian:11
        command:
        - sleep
        - "36000"
        resources:
          requests:
            cpu: "2000m"       # Minimum CPU requirement
            memory: "128Mi"   # Minimum memory requirement
```
In this YAML file, we define a Deployment with a single replica, using the debian:11 image. The resources section specifies the CPU and memory limits and requests for the container.

Step 2: Apply the Deployment
Apply the Deployment YAML file to create the Deployment. Run the following command:

```
kubectl apply -f demo-deployment.yaml
```
This will create the Deployment with the specified resource allocations.

Step 3: Verify the Deployment
To verify the resource allocations of the Deployment, you can describe the pod and inspect the resource information. Run the following command:

```
kubectl describe pod <pod-name>
```
Replace <pod-name> with the actual name of the pod created by the Deployment.

Is the pod started?  
What is happening?  
What can we do to solve the problem?

