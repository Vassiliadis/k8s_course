
Step 1: Create an NGINX Pod using the kubectl create command:


```
kubectl run nginx-pod --image=nginx:1.25
```
This command creates a Pod named nginx-pod using the nginx:1.25 image.

Step 2: Verify that the Pod is running:


```
kubectl get pods
```
You should see the nginx-pod in the list of Pods with a status of "Running".

Step 3: Create a Service to expose the NGINX Pod:

```
kubectl expose pod nginx-pod --name mywebserver --type ClusterIP --port 80
```
This command creates a Service named mywebserver of type ClusterIP, exposing port 80 on the Pod.


Step 4: Verify the Service creation and its assigned node port:

```
kubectl get -o wide services
```
You should see the mywebserver service in the list of Services.

Step 5: Access the NGINX service by running a new pod:


```
kubectl run --image ubuntu:23.04 -it --image-pull-policy IfNotPresent --restart Never ubuntu-$RANDOM


apt update # update pkg list
apt install -y curl bind9-host # install tools

curl http://mywebserver
host mywebserver
```

Closer look into DNS:
```
host -v mywebserver
exit # leave the ubuntu pod
```

Kubernetes has created a DNS entry for our service and we can use it as a stable reference to our pod's web port - or any other configured port.

Yet, this service is available only within our cluster!


