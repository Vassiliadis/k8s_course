Step 1: Run the Ubuntu Container  
Open a terminal and run the following command:

```
docker run -it --rm ubuntu
```
This command pulls the ubuntu image from the Docker Hub and runs it as an interactive container. The --rm option automatically removes the container after it exits.

You will now be inside the Ubuntu container's shell prompt.

Step 2: Examine Networking Information  
You can now use various commands to examine networking information. Here are some examples:

To display network connections:

```
ss -an
```
To show IP addresses assigned to network interfaces:

```
ip addr
```
To display the routing table:

```
ip route
```

Why did the above commands fail?
