Docker Demo: Nginx Image with Default Page

Step 1: Run the Nginx Container  
Open a terminal and run the following command:


```
docker run -p 80:80 nginx
```
This command pulls the nginx image from the Docker Hub and runs it as a container. The -p option maps port 80 of the container to port 80 of your machine, allowing you to access the Nginx server.

If everything is set up correctly, you should see the following output:

## Welcome to nginx!  
If you see this page, the nginx web server is successfully installed and working. Further configuration is required.

For online documentation and support please refer to nginx.org.
Commercial support is available at nginx.com.

Thank you for using nginx.

***

Congratulations! You have successfully run a Docker container using the Nginx image, and you can now access the default Nginx page by opening your web browser and navigating to http://172.19.1.120 (use the correct IP!).

