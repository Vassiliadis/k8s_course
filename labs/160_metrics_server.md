# K8s Metrics Server

Install metrics server 0.6.3:
```
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.6.3/components.yaml
```

We need to modify the arguments that the metrics server is using:
```
kubectl get -n kube-system deployment -o yaml metrics-server | yq '.spec.template.spec.containers[0].args' 
```

This will print out something like this:
```
[
  "--cert-dir=/tmp",
  "--secure-port=4443",
  "--kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname",
  "--kubelet-use-node-status-port",
  "--metric-resolution=15s"
]
```

We need to add '--kubelet-insecure-tls'. The correct path is:
.spec.template.spec.containers[0].args


Edit the deployment, locate where the arguments are set and add '--kubelet-insecure-tls':

```
kubectl edit deployment -n kube-system metrics-server
```

Verify:
```
kubectl get -n kube-system deployment -o yaml metrics-server | yq '.spec.template.spec.containers[0].args' 
```

Wait for the metric server to start:
```
kubectl get -n kube-system deploy metrics-server
```
It should print something like this:
```
NAME             READY   UP-TO-DATE   AVAILABLE   AGE
metrics-server   1/1     1            1           9h
```

Now, you can use:
```
kubectl top node
kubectl top pod
```
