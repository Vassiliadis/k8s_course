# Part 1: Static pods

Create a new pod defintion using '--dry-run -o yaml':
```
kubectl run -n default --dry-run my-static-pod --image nginx:1.24 -o yaml > /tmp/apod.yaml
```

What did we do?  
Did we create a pod?  

```
kubectl get -n default get pod | grep static-pod
```

You should see no pod! We used 'kubectl run ... --dry-run'.

---

OK. Let's copy this pod definition to /etc/kubernetes/manifests/:
``` 
cp /tmp/apod.yaml /etc/kubernetes/manifests/nginx.yaml
```

Examine again pods:
```
kubectl get -n default get pod | grep static-pod
```

What has happened?

---


# Part 2: ETCD restore

Create a very important namespace and a few very important pods:
```
kubectl create ns myimportantns
for num in $(seq 10)
do
  kubectl run -n myimportantns --image alpine alpine-$num --command sleep 36000
  kubectl expose -n myimportantns pod alpine-$num --port 80 --type NodePort
done


```

Check what is there:
```
kubectl get -n myimportantns all
```


We have 10 pods running and they are in production.  There are exposed services as well as NodePort services.

---

 Wait until all pods are in Running state!

---

Trigger an etcd backup:
```
sh -x /root/bin/backup_etcd.sh
```

Notice the name of the backup file we just created!

Now, go ahead and delete a pod from namespace myimportantns:
```
kubectl delete ns myimportantns
```
---
# That was really stupid! 
# Namespace 'myimportantns' is gone!
# all pods and services are gone!
---


But we do have our backup. Restore the backup inside /tmp:

```
tar -C /tmp -xvf /var/backups/etcd/etcd.2023-06-15_17\:00.tar.gz
```


Now, move it to /var/lib/etcdrestore/:
```
# DEADLY HINT. DO NOT RESTORE inside /var/lib/etcd/. Kubernetes is using it as we have not stopped it. NEVER DO THIS. USE ANOTHER DIRECTORY.

mv -iv /tmp/var/lib/etcd/ /var/lib/etcdrestore/
```


Ok. Now, let's try to use the new directory.  
Step 0, check the etcd process:
```
pgrep etcd
```

Step 1, open /etc/kubernetes/manifests/etcd.yaml  
Step 2, find where volume related configuration is  
Something like:
```
  volumes:
  - hostPath:
      path: /etc/kubernetes/pki/etcd
      type: DirectoryOrCreate
    name: etcd-certs
  - hostPath:
      path: /var/lib/etcd
      type: DirectoryOrCreate
    name: etcd-data

```

Step 3, change the path from '/var/lib/etcd' to '/var/lib/etcdrestore' 

Step 4, check the etcd process:
```
pgrep etcd
```

The old etcd is probably already dead or it will be dead soon.
Wait for an etcd process to re-appear.



ok. An etcd process is running. Kuberenetes should be now OK. Check if the lost namespace is back.
```
kubectl get -n myimportantns all
```

Hint: Document this change for other people to know:
```  
'/var/lib/etcd' to '/var/lib/etcdrestore' 
```
Or ideally fix the issue by removing /var/lib/etcd and renaming etcdrestore to etcd.

