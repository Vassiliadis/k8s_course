Save this to a file and create the resource using kubectl:
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: cpuhog
  namespace: default
spec:
  replicas: 2
  selector:
    matchLabels:
      app: cpuhog
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: cpuhog
    spec:
      containers:
      - command:
        - dd
        - if=/dev/urandom
        - bs=100000000
        - of=/dev/null
        image: debian:11
        imagePullPolicy: IfNotPresent
        name: burn-cpu-0
        resources:
          limits:
            cpu: 850m
      - command:
        - dd
        - if=/dev/urandom
        - bs=100000000
        - of=/dev/null
        image: debian:11
        imagePullPolicy: IfNotPresent
        name: burn-cpu-1
        resources:
          limits:
            cpu: 850m
      - command:
        - dd
        - if=/dev/urandom
        - bs=100000000
        - of=/dev/null
        image: debian:11
        imagePullPolicy: IfNotPresent
        name: burn-cpu-2
        resources:
          limits:
            cpu: 850m
      - command:
        - dd
        - if=/dev/urandom
        - bs=100000000
        - of=/dev/null
        image: debian:11
        imagePullPolicy: IfNotPresent
        name: burn-cpu-3
        resources:
          limits:
            cpu: 850m
```


Examine:
```
kubectl get deploy
kubectl get replicaset
kubectl get -o wide pod
```

Use the metrics server!
```
kubectl top node
kubectl top pod
```
