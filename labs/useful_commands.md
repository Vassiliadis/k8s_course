Get all resource names (namespaces and services in this example) to use programmatically:
```
kubectl get namespaces -o=jsonpath='{.items[*].metadata.name}'
kubectl get svc -o=jsonpath='{.items[*].metadata.name}'
```

Get pods in namespace 'my-ns':
```
kubectl get -n my-ns pods
```

Get resource (pods in this example) across namespaces:
```
kubectl get -A pods
```

Get more information for a resource name (pods in this example):
```
kubectl get -o wide pods
kubectl get -o yaml pods | less
kubectl get --show-labels pods
kubectl describe pods | less
```

Get several (not all) resource types:
```
kubectl get all
kubectl get all,cm,secret,ing 
```

Delete all pods in the 'default' namespace:
```
kubectl delete -n default pods --all
``` 

Using yq (or jq) to filter results:`
```
kubectl get -n kube-system -o yaml pod | yq '.items[].status.podIP'
kubectl get -n kube-system -o json pod | jq '.items[].status.podIP'
```

https://kubernetes.io/docs/reference/kubectl/cheatsheet/


