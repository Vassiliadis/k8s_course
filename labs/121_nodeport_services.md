Step 1: Create an NGINX Pod named 'frontend':

```
kubectl run frontend --image=nginx:1.25
```
This command creates a Pod named 'frontend' using the 'nginx:1.25' image.

Step 2: Verify that the Pod is running:

```
kubectl get pods
```
You should see the 'frontend' Pod in the list of Pods with a status of "Running".

Step 3: Create a NodePort Service to expose the NGINX Pod:



```
kubectl expose pod frontend --type NodePort --port 80
```

Kubernetes will create a NodePort service and expose the pod's TCP port 80 inside the cluster. The name of the service will be the name of the pod but this can be configured using "--name thenamethatidesire".

Step 4: Verify the Service creation and its assigned node port:

```
kubectl get -o wide svc frontend
```

This will print something like this:
```
NAME       TYPE       CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE   SELECTOR
frontend   NodePort   10.109.150.215   <none>        80:31004/TCP   29m   run=frontend
```

In this example, we didn't specify a port and kubernetes chose 31004. This can be explicitly defined - within the service node port range which defaults to 30000-32767.


Now, the NGINX Pod named 'frontend' is accessible using the assigned node port on any node in the Kubernetes cluster. You can access the NGINX service using the node's IP address and the assigned node port (e.g., http://<node-ip>:31004).


```
kubectl get node -o wide # examine the INTERNAL-IP column

curl http://172.19.1.130:31004 # use all three IP addresses from the previous command and the correct tcp port
curl http://172.19.1.131:31004 # use all three IP addresses from the previous command and the correct tcp port
curl http://172.19.1.132:31004 # use all three IP addresses from the previous command and the correct tcp port

```

Note that a NodePort service is also available within the cluster with a proper DNS name like a ClusterIP service!  
Use the commands from the clusterip_service lab to examine DNS etc.

Yet, this time our service is availabe outside the cluster as well! We can route traffic to our Kubernetes cluster!

Use your browser to access the service!
