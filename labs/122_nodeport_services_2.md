Out network administrator has asked as to use port 32000 for our NodePort service named frontend!

Use:
```
EDITOR=nano kubectl edit svc frontend # edit using nano
# To use the default editor (vi):
kubectl edit svc frontend
```

"kubectl edit" will let us edit a Kubernetes resource using an editor. Some resources are immutable!  

Search for nodePort and change its value to 32000.


Examine services again and notice the difference.
```
kubectl get svc
# Or:
kubectl describe svc | less -i
```
