# Pods

In Kubernetes, a Pod is the smallest and simplest unit of deployment. It represents a single instance of a running process or a group of tightly coupled processes that share the same network namespace, IPC, and storage within a node in the cluster. Pods are the basic building blocks for running containers in Kubernetes.

Here are some key aspects of Kubernetes Pods:

Atomic unit: A Pod is an atomic unit of deployment, meaning it cannot be divided further. It represents a single logical application or a component of an application. It encapsulates one or more containers along with shared resources and configuration.

Group of containers: A Pod can consist of one or more containers that are co-located and share the same lifecycle. These containers are scheduled together on the same node and can communicate with each other using localhost. Containers within a Pod are typically tightly coupled and work together to fulfill a specific purpose.

Shared resources: Pods share certain resources, such as network namespace and IP address. Each Pod has a unique IP address within the cluster, allowing containers within the Pod to communicate with each other using localhost. Pods can also share volumes, which provide shared storage that can be accessed by the containers within the Pod.

Pod lifecycle: Pods have their own lifecycle managed by the Kubernetes control plane. They are created based on Pod definitions, scheduled to nodes, and continuously monitored for health. If a Pod fails, the control plane can automatically restart or reschedule it on a different node to maintain the desired state.

Labels and selectors: Pods can be labeled with key-value pairs to enable grouping and selection. Labels are used for organizing and managing Pods, and they can be used in conjunction with selectors to select and manage Pods as a group. This allows for more granular control over scheduling, scaling, and managing Pods within the cluster.

Pod communication: Pods can communicate with each other directly using their IP addresses and port numbers. They can also communicate with other Pods or services in the cluster using Kubernetes Services, which provide a stable network endpoint and load balancing.

Pods are typically not created directly by users but are managed by higher-level controllers like Deployments, ReplicaSets, or StatefulSets. These controllers handle the creation, scaling, and monitoring of Pods based on the desired state specified in their configurations.

Overall, Pods in Kubernetes provide an abstraction layer for running and managing containers, allowing for better resource sharing, encapsulation, and scalability within the cluster.



# Pod networking

In Kubernetes, pods are assigned an IP address within the cluster's network. However, it's important to note that the IP address assigned to a pod is not permanent or externally accessible. The IP address is typically assigned dynamically by the underlying networking solution in the Kubernetes cluster.

Here are some points to consider regarding IP addresses for pods:

Internal cluster communication: Each pod in the cluster is allocated a unique IP address within the cluster's network. This allows pods to communicate with each other using their assigned IP addresses. Other pods and services within the cluster can reach a pod using its IP address and the appropriate port.

Dynamic IP assignment: The IP address assigned to a pod is dynamic and subject to change. If a pod fails or is terminated and then restarted, it may receive a different IP address. This dynamic nature allows for flexibility in scheduling and managing pods within the cluster.

Pods and external communication: By default, pods are not directly reachable from outside the cluster. They are part of an internal network created by the Kubernetes cluster. If you need to expose a pod externally, you can use Kubernetes Services, which provide a stable network endpoint with a load balancer or NodePort configuration.

Pod-to-pod communication: Pods can communicate with each other using their IP addresses and port numbers. They can use localhost or the pod's IP address to establish connections with other pods running within the cluster.

It's worth noting that in certain Kubernetes networking configurations, such as network overlays or specific network plugins, the networking setup might differ. However, the concept of pods having an IP address for internal cluster communication remains consistent.

Overall, while pods are assigned IP addresses within the cluster, these addresses are not meant to be externally accessible or relied upon as fixed addresses. The dynamic allocation of IP addresses allows for flexible scheduling and management of pods within the Kubernetes cluster.





