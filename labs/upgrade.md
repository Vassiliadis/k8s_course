1. Check kubeadm version:  
```kubeadm version```


2. Unhold the package to prepare for upgrade. We need to have the repo installed to have all available versions.
```
apt-mark unhold kubeadm
apt-get update && apt-get install -y kubeadm=1.27.x-00
apt-mark hold kubeadm
```

3. Check upgrade plan:  
``` kubeadm upgrade plan```

4. Run the upgrade:  
```kubeadm upgrade apply v1.27.0```

5. CNI upgrade? 

6. Upgrade the rest of the nodes:  
```kubeadm upgrade node```

7. Drain the nodes to prepare the rest of the upgrade
```kubectl drain <node> --ignore-daemonsets```

8. Upgrade kubelet and kubectl like kubeadm:
```
apt-mark unhold kubelet kubectl
apt-get update && apt-get install -y kubelet=1.27.x-00 kubectl=1.27.x-00
apt-mark hold kubelet kubectl
```

9. Restart kubelet:
```
systemctl daemon-reload
systemctl restart kubelet
```

10. Reenable the node:  
```kubectl uncordon <node>```

