Step 1: Create a file named nginx-daemonset.yaml and add the following content:
```
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: nginx-daemonset
spec:
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
        - name: nginx
          image: nginx:1.24.0
```


Step 2: Apply the DaemonSet configuration:
```
kubectl apply -f nginx-daemonset.yaml
```
Step 3: Verify the DaemonSet and the associated Pods:


```
kubectl get daemonset
kubectl get pods
```

Step 4: Confirm that the nginx Pods are running on each node:


```
kubectl get pods -o wide
```

The DaemonSet ensures that an instance of the nginx:1.24.0 container runs on each node in the cluster. The nginx-daemonset.yaml file defines the DaemonSet with a selector to match Pods with the app=nginx label. Each node in the cluster will have a corresponding Pod running the nginx container.

You can modify the YAML file to suit your needs, such as adjusting the image version, adding additional specifications to the Pod template, or configuring resource limits and requests for the containers.
