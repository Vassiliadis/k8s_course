Step 1: Create the web Namespace:

```
kubectl create namespace web
```
Step 2: Create the db Namespace:

```
kubectl create namespace db
```
Step 3: Deploy NGINX in the web Namespace:

```
kubectl create deployment nginx-deployment --image=nginx --namespace=web
```
Step 4: Deploy Redis in the db Namespace:

```
kubectl create deployment redis-deployment --image=redis --namespace=db
```
Step 5: Verify the Deployments in their Respective Namespaces:

```
kubectl get deployments -n web
kubectl get deployments -n db
```

You should see the NGINX deployment in the web namespace and the Redis deployment in the db namespace.

Examine all namespaces:
```
kubectl get ns
```

You can use '-A' to examine resources across namespaces:
```
kubectl get -A pod
kubectl get -A deploy
kubectl get -A svc
```

Hint!  
Deleting a namespace is an easy way to delete all resources living in it.  
But, some resources are cluster-wide!  
Take a look:
```
kubectl api-resources | less -i
```
