Step 1: Run the Ubuntu Container  
Open a terminal and run the following command:
```
docker run -it --rm ubuntu
```
This command pulls the ubuntu image from the Docker Hub and runs it as an interactive container. The --rm option automatically removes the container after it exits.

You will now be inside the Ubuntu container's shell prompt.

Step 2: Examine System Resources  
You can use various commands available in Ubuntu to examine system resources. Here are some examples:

To list running processes:
```
ps axu
```
To display system-wide information, including CPU and memory usage:
```
top
```
To display system-wide memory usage:
```
free -h
```
To show disk space usage:

```
df -h
```
To display mounted filesystems:

```
mount
```
To list block devices:

```
lsblk
```
Feel free to explore other commands available in Ubuntu for examining system resources.

Step 3: Exit the Container
To exit the Ubuntu container, simply type exit or press Ctrl + D.

That's it! You have successfully run a Docker container using the Ubuntu image and examined system resources using commands like ps, top, df -h and mount.
