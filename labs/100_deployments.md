Step 1: Create a file named nginx-deployment.yaml and add the following content:


```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
        - name: nginx
          image: nginx:1.24.0
```

Step 2: Apply the Deployment configuration:

```
kubectl apply -f nginx-deployment.yaml
```

Step 3: Verify the Deployment, the associated ReplicaSet, and the Pods:

```
kubectl get deployment
kubectl get replicaset
kubectl get pods
```
Step 4: Scale the Deployment to a different number of replicas. Modify the replicas field in nginx-deployment.yaml to the desired number, and apply the changes again:

```
kubectl apply -f nginx-deployment.yaml
```

Step 5: Verify the updated number of replicas:


```
kubectl get deployment
kubectl get replicaset
kubectl get pods
```
The Deployment ensures that the specified number of replicas of the nginx:1.24.0 container are running and maintained in the cluster. The nginx-deployment.yaml file defines the Deployment with a selector to match Pods with the app=nginx label. The template specifies the configuration of the Pods, including the container specifications.

You can modify the YAML file to suit your needs, such as adjusting the number of replicas, updating the image version, adding labels or annotations, or configuring resource limits and requests for the containers.




