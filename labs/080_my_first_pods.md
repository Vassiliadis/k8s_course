Let's run our first pod!

My first pod using YAML:
```
apiVersion: v1
kind: Pod
metadata:
  name: first-pod
spec:
  containers:
    - name: hello-container
      image: nginx:latest
```
In this example:

The apiVersion specifies the Kubernetes API version.  
The kind is set to Pod to indicate that we're defining a Pod resource.  
The metadata section includes the name of the pod, which is set to first-pod in this case.  
The spec section defines the specification for the pod.
Under containers, we define a single container named hello-container.  
The container uses the nginx:latest image from Docker Hub.

To deploy the pod:

Save the YAML configuration to a file, e.g., first-pod.yaml.
Apply the configuration using the kubectl apply command:

```
kubectl apply -f first-pod.yaml
```

Kubernetes will create the pod based on the provided configuration. The pod will run a single container using the specified image (in this case, Nginx). Since no specific ports are exposed, the container will still be accessible within the pod's network namespace.


Get the pod's IP address and try to access the service using curl:
```
kubectl get -o wide pod
curl http://10.244.2.7
```

Something like below:
```
root@debian130.manager:~$ kubectl get -o wide pod
NAME        READY   STATUS    RESTARTS      AGE   IP           NODE                          NOMINATED NODE   READINESS GATES
first-pod   1/1     Running   1 (51m ago)   55m   10.244.2.7   debian132.worker.none.local   <none>           <none>
root@debian130.manager:~$ curl http://10.244.2.7
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
root@debian130.manager:~$
```

Now, we can try achieving the same using imperative commands:
```
kubectl run --image nginx:latest first-pod
```

Did it work? We already have a pod with that name and we need to delete it:
```
kubectl delete pod first-pod
```

Try again!  
Did it work?

Important note:  
This nginx web service is not accessible outside the cluster.


