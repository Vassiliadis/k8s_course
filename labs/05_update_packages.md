Step 1: Run the Ubuntu Container  
Open a terminal and run the following command:

```
docker run -it --rm ubuntu
```
This command pulls the ubuntu image from the Docker Hub and runs it as an interactive container. The --rm option automatically removes the container after it exits.

You will now be inside the Ubuntu container's shell prompt.

Step 2: Perform System Updates  
Run the following commands to update the package lists and upgrade the installed packages:

```
apt update
apt upgrade -y
```
This will update the package lists and install any available updates.

Step 3: Install iproute2 Package  
Run the following command to install the iproute2 package:

```
apt install -y iproute2
```
This will install the iproute2 package, which provides advanced networking tools.

Step 4: Examine Networking Information  
You can now use various commands to examine networking information. Here are some examples:

To display network connections:

```
ss -an
```
To show IP addresses assigned to network interfaces:

```
ip addr
```
To display the routing table:

```
ip route
```
Feel free to explore other networking commands available in the iproute2 package.

Step 5: Exit the Container
To exit the Ubuntu container, simply type exit or press Ctrl + D.

That's it! You have successfully run a Docker container using the Ubuntu image, performed system updates, installed the iproute2 package, and examined networking information using commands like ss -an, ip addr, and ip route.


