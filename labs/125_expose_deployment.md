Let's create a deployment and two services for it.

Step 1: Create the Deployment:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.24.0
```
Save the above YAML in a file named nginx-deployment.yaml and create the Deployment:

```
kubectl create -f nginx-deployment.yaml
```

Step 2: Create a Headless Service:

```
apiVersion: v1
kind: Service
metadata:
  name: nginx-headless-service
spec:
  clusterIP: None
  selector:
    app: nginx
  ports:
  - protocol: TCP
    port: 80
```
Save the above YAML in a file named nginx-headless-service.yaml and create the Headless Service:


Step 3: Create a ClusterIP Service:

```
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
spec:
  selector:
    app: nginx
  ports:
  - protocol: TCP
    port: 80
```
Save the above YAML in a file named nginx-service.yaml.

Step 4: Create both services

Step 5: Verify the Deployment and the creation of the services:

```
kubectl get deployments
kubectl get -o wide pods
kubectl get -o wide services
kubectl describe services nginx-headless-service
kubectl describe services nginx-service
```

Examine these two services using DNS from a Ubuntu pod and notice the difference between the clusterip and the headless service. The clusterip service resolves to one IP address and the headless service resolves to the number of replicas, 3 IPs in this example.
