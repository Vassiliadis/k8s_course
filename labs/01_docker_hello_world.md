Docker Demo: Hello World

Step 1: Run the Hello World Container  
Open a terminal and run the following command:

```
docker run hello-world
```
This command pulls the hello-world image from the Docker Hub and runs it as a container. The container will print a "Hello from Docker!" message and then exit.

If everything is set up correctly, you should see the following output:


```
Hello from Docker!
This message shows that your installation appears to be working correctly.
...
```

Congratulations! You have successfully run a Docker container using the hello-world image.

Note: The hello-world image is a simple, lightweight image provided by Docker to verify that your installation is working correctly. It serves as a useful starting point for learning Docker.


