Static provisioning using CSI (nfs-csi).
===


Step 1: Get a template in order to create a persistent volume:

```
wget https://raw.githubusercontent.com/kubernetes-csi/csi-driver-nfs/master/deploy/example/pv-nfs-csi.yaml
```

Step 2: Make changes to the template:
```
    volumeAttributes:
      server: 172.19.1.110
      share: /nfs/static-volume
```

Step 3: Create the PV using 'kubectl'.

Step 4: Create a persistent volume claim:
```
# kubectl -f accepts URLs too!
# Since we don't need to change anything, we can just create it. 
kubectl create -f https://raw.githubusercontent.com/kubernetes-csi/csi-driver-nfs/master/deploy/example/pvc-nfs-csi-static.yaml
```

Step 5: Examine the resources you have created:
```
kubectl get -o wide pv,pvc

kubectl describe pv,pvc
```

Step 6: Create a pod using the PVC we've just created:

```
kind: Pod
apiVersion: v1
metadata:
  name: my-pod
spec:
  containers:
    - name: my-frontend
      image: nginx
      volumeMounts:
      - mountPath: "/usr/share/nginx/html"
        name: my-csi-volume
  volumes:
    - name: my-csi-volume
      persistentVolumeClaim:
        claimName: pvc-nfs-static
```

When the pod referencing a CSI volume is scheduled, Kubernetes will trigger the appropriate operations against the external CSI plugin (ControllerPublishVolume, NodeStageVolume, NodePublishVolume, etc.) to ensure the specified volume is attached, mounted, and ready to use by the containers in the pod.

Use curl to examine the default page served by NGINX!

In this lab, we have seen how we can use a volume with permanent data from a pod. This time, we have used NFS but CSI offers many options:


https://kubernetes-csi.github.io/docs/drivers.html



