Step 1: Create a Static Website

Create a new directory and navigate to it using "cd $(mktemp -d)". Inside the directory, create a folder called html to hold your static website files. Place your HTML, CSS, JavaScript, or any other static content in this folder. For example, create an index.html file inside the html folder with the following content:

```
<!DOCTYPE html>
<html>
<head>
    <title>My Dockerized Website</title>
</head>
<body>
    <h1>Welcome to my Dockerized website!</h1>
    <p>This website is served using Nginx in a Docker container.</p>
</body>
</html>
```

Step 2: Create a Dockerfile

In the same directory as your html folder, create a file called Dockerfile (no file extension). Open it in a text editor and add the following content:

```
FROM nginx:latest

COPY html /usr/share/nginx/html

EXPOSE 80
```

This Dockerfile specifies the base image as nginx:latest, copies the contents of the html folder to the appropriate location in the container (/usr/share/nginx/html), and exposes port 80.

Step 3: Build the Docker Image

Open a terminal and navigate to the project directory. Run the following command to build the Docker image:

```
docker build -t nginx-demo .
```

This command tells Docker to build an image using the Dockerfile in the current directory. The -t option specifies the name and tag for the image (nginx-demo in this case).

Step 4: Run the Docker Container

After the image is built, you can run it in a Docker container using the following command:


```
docker run -d -p 80:80 nginx-demo
```

This command tells Docker to run a container using the nginx-demo image, detach it from the terminal (-d option), and map port 80 of the container to port 80 of your machine.

Step 5: Access the Website

Open your web browser and navigate to http://172.19.1.120 (use the correct IP). You should see the static website you created displayed. The Nginx server running inside the Docker container is now serving your website.

That's it! You have created a Docker demo using the Nginx image to serve a static website. You can modify the content of the html folder and repeat steps 3 and 4 to rebuild and run the updated Docker image.

