# Taints and tolerations

Step 1: Taint one of the worker nodes:


```
kubectl taint nodes <node-name> disk=slow:NoSchedule
```
Replace <node-name> with the name of the node you want to taint.

Step 2: Run a few pods and examine where they have been scheduled:
```
kubectl run --image registry.k8s.io/pause pause-$RANDOM
kubectl run --image registry.k8s.io/pause pause-$RANDOM
kubectl run --image registry.k8s.io/pause pause-$RANDOM
kubectl run --image registry.k8s.io/pause pause-$RANDOM
kubectl get -o wide pod
```

Step 3: Create the Pod YAML File:  
Create a file named pod.yaml with the following content:

```
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: nginx
    image: nginx
  tolerations:
  - key: disk
    operator: Equal
    value: slow
    effect: NoSchedule
```
In the above YAML, we define a pod named my-pod with a single container running the NGINX image. We also specify a toleration for the disk=slow:NoSchedule taint.

Step 4: Create the Pod:

```
kubectl create -f pod.yaml
```
This command creates the pod using the YAML file. The pod will be possibly scheduled onto the tainted node because it has a matching toleration for the taint.

Step 5: Verify the Pod and Node Status:

```
kubectl get -o wide pods
kubectl describe node <node-name>
```
You should see the NGINX pod in the Running state and is possibly scheduled on the tainted node. Additionally, the node description should show the taint disk=slow:NoSchedule and the toleration from the pod.

All pause pods should be scheduled on other nodes. 

By using the toleration, the pod can be scheduled onto the tainted node despite the taint's presence. This allows you to control the placement of pods and manage resource constraints within your Kubernetes cluster.



https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/




