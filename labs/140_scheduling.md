Create a YAML definition of an NGINX pod:
```
fl=$(mktemp) # create a new file in /tmp/
kubectl run web --image=nginx --dry-run=client -o yaml > $fl
echo $fl
cat $fl
```

This command will generate the YAML representation of the pod without actually creating it. You can view the generated YAML output in the terminal.

Note that the --dry-run option is used to simulate the creation of the resource without making any changes to the cluster. The -o yaml option is used to output the generated YAML to the terminal.

Once you have the YAML representation, you can modify it if needed and then apply it to create the pod using the kubectl apply -f <yaml-file> command.
```
kubectl apply -f yaml-file
```

Keep in mind that the kubectl run command is primarily intended for quickly creating simple deployments, and for more complex scenarios, using separate YAML files is recommended.




Edit the file and under 'spec:' add 'nodeName: your.manager.node'.
Something like:
```
spec:
  nodeName: debian130.manager.none.local
```


Then re-create the pod (replace is a shortcut for delete and create):
```
kubectl replace --force -f $fl
```

Check where it is scheduled:
```
kubectl get -o wide pod
```

Change the nodeName field again, re-create the pod and check it again.

