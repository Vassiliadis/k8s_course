# Logs

Start by deploying a sample Deployment. You can use the following YAML manifest to create a Deployment with an Nginx container and a Redis container:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: demo-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: demo
  template:
    metadata:
      labels:
        app: demo
    spec:
      containers:
        - name: nginx
          image: nginx
        - name: redis
          image: redis
```
Save the above YAML manifest to a file named demo-deployment.yaml, and then create the Deployment by running the following command:

```
kubectl apply -f demo-deployment.yaml
```
Once the Deployment is running and the Pods are created, you can use kubectl logs to view the logs for each container. Run the following command to view the logs from the Nginx container:
r
```
kubectl logs -l app=demo -c nginx
```
This command fetches the logs from the Nginx container in all Pods with the label app=demo.

Similarly, you can view the logs from the Redis container by running the following command:
r
```
kubectl logs -l app=demo -c redis
```
This command fetches the logs from the Redis container in all Pods with the label app=demo.

If you want to continuously stream the logs in real-time, you can use the -f flag. For example, to stream the logs from the Nginx container, run the following command:
```
kubectl logs -l app=demo -c nginx -f
```
You can also specify a specific Pod to fetch the logs from using its name. For example, to view the logs from the Nginx container in a specific Pod, run the following command:
```
kubectl logs pod-name -c nginx
```
Replace pod-name with the name of the Pod you want to view logs from.
