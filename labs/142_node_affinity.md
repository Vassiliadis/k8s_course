# Node affinity


Step 1: Label Nodes

Label the Kubernetes nodes with the appropriate storage labels. Run the following commands to label the nodes:



```
kubectl label nodes node-name-1 storage=fast
kubectl label nodes node-name-1 DC=Japan

```

Replace node-name-1 with the actual name of a worker node.

Step 2: Create a few pods with Node Affinity

Create a file named demo-pod.sh with the following content:

```
#!/usr/bin/env bash
kubectl apply -f - <<END
apiVersion: v1
kind: Pod
metadata:
  name: demo-pod-$RANDOM
spec:
  containers:
  - name: nginx
    image: nginx
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: storage
            operator: In
            values:
            - fast
END
```

In this YAML definition, we define a Pod with an nginx container. The affinity section specifies the node affinity using the "storage" label with the value "fast". This ensures that the Pod will be scheduled only on nodes labeled with "storage=fast".

Step 3: Apply the Pod
Apply the Pod YAML file to create Pods. Run the following commands:

```
bash demo-pod.sh # run it 5 times
```
This will create Pods and they will be scheduled only on a node with the "storage=fast" label.

Step 4: Verify Pod Placement
To verify that Pods are scheduled correctly based on the node affinity, run the following command:

```
kubectl get pod -o wide
```
You should see Pods running on a node with the "storage=fast" label.


---
Step 5: Try the 'NotIn' operator

Change the operator to 'Notin', change key to 'DC' and value to 'Japan'

Run a few pods and check the results. Our pods ahould avoid this DC.

---
Other operators:

https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#operators

---
Using node affinity we can influence the Kubernetes scheduler!
